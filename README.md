# CountedBackup
A lightweight backup program meant to be automated in cron.

By Rory Landau

For [NeXbooks](https://www.nexbooks.org/)

## Usage
Countedbackup is a lightweight backup program meant to be executed on schedule.
This enables cron to keep multiple, flexible backups with one line.
Relatedly, CountedBackup is an exercise in efficiency;
it runs quickly and quietly in environments where background programs eating up
system resources is less than ideal (read: a production server).

**It was built for Unix webservers.**
**Has not been tested in a Windows environment.**

## Syntax
`countedbackup [1] [2] [3]`

1. maximum number of backups to be kept within the backup dir (0<x<98)
2. the target directory to be tar/gzip'd (backed up)
3. the destination/backup directory that is being maintained by the program
    Call with -h or --help to see more info.


## NOTE
The program acts in an unorthodox manner when hitting the backup limit (100).
One(!) old backup (99) is renamed to "pre-loop.tar.gz" and the backup number
(the prepended number) is reset to 1. This means that it is possible for one
over the specified number of backups to exist, as protected by rename.
This pre-loop is then deleted in purgeBackups() when any backup is deleted
(aka the purge limit is non-zero).
This is because, if newer backups are being deleted,
certainly the pre-loop backup should be removed.
This could and should be fixed and streamlined, but it works as long as your
storage capacity isn't going to be overwhelmed by an extra backup.
This also means there is a weakness in the number of backups when the loop is
hit. However, at least 2 will exist, which should be sufficient (if not ideal).
