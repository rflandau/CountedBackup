/* A simple program meant to automate tar/gzip'ing a directory to act as a
backup. The program checks the destination directory for how many similarly-
named files there are and deletes the oldest one if there are too many (as
determined by the number given as arguemnt when the program is called).
Meant to be executed in cron at your chosen frequency and piped to (a) log(s).
Ought to be kept very lightweight as to not weight down the server.

By Rory Landau
For Brainstorms Nextbooks*/
//TODO print debug statements to a log file.
//TODO reduce memory usage by making more allocs form-fitting
//TODO replace system() call
//TODO check backups by timestamp instead of filename
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#define BUF_CAP 100
/* BUF_CAP is set fairly arbitrarily, but is low to conserve
server memory. There is a chance this will causes overflow issues on bigger
file trees */
#define BACKUP_LIMIT 100
#define FULL_PURGE -1000

/* Ensures the arguments are satisfactory.
Kills the program if they are not or the help flag is ticked. */
int checkArgs(int argc, char* argv[]){
  int toReturn;

  if(argc != 4){ //argument count check
    if(argc == 2 && (strcmp(argv[1], "-h") || strcmp(argv[1], "--help"))){
      //help output
      fprintf(stdout, "A simple program meant to automate tar/gzip'ing a "
      "directory to act as a backup. The program checks the destination "
      "directory for how many similarly-named files there are and deletes the "
      "oldest one if there are too many (as determined by the number given as "
      "arguemnt when the program is called).\n"
      "Meant to be executed in cron at your chosen frequency and piped.\n"
      "Ought to be kept very lightweight as to not weight down the server.\n"
      "\n"
      "By Rory Landau\n"
      "For Brainstorms Nextbooks\n");
    }
    fprintf(stdout, "Syntax: countedbackup <1> <2> <3>\n"
      "1: maximum number of backups to be kept within the backup dir (0<x<%d)\n"
      "2: the target directory to be tar/gzip'd (backed up)\n"
      "3: the destination/backup directory that is being maintained by the"
      " program\n Call with -h or --help to see more info.\n", BACKUP_LIMIT-2);
    exit(0);
  }
  sscanf(argv[1], " %d", &toReturn); //convert count to an int
  //check given maximum
  if(toReturn < 1){ //too low
    fprintf(stdout, "You want to keep fewer than 1 backups?\n"
    "That doesn't even make any sense.\n"
    "Exitting...\n");
    exit(0);
  } else if(toReturn > BACKUP_LIMIT-2){ //too high including buffer of 2
    fprintf(stdout, "It is inadvisable to set a maximum of >%d backups "
    "due to the way backups are stored after hitting number %d and will "
    "definitely cause some artefacting.\n"
    "If you need this many backups, recompile with a higher backup limit (as "
    "found and set in constructCommand()).\n"
    "Exitting...\n", BACKUP_LIMIT-2, BACKUP_LIMIT);
    exit(0);
  }
  return toReturn;
}
/* Attempts to open the given path as a directory.
Kills the program if the subroutine fails.
Returns a valid directory.*/
DIR* tryDir(char* path){
  DIR* dir;

  dir = opendir(path);
  if(dir == NULL){ //if dir returned null, the directory is inaccessible
    fprintf(stderr, "ERROR @ tryDir. Exitting with error:\n"
    "    %s!\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  return dir;
}
/* Lists all the entries within the given directory, finding and returning the
total number of backups ([0]) as well as the number of the newest backup (as
determined by the prepended number) ([1]). Though only the highest backup is
returned, the program fully parses every piece of the backup name, meaning other
pieces could be returned.
@params - DIR* dir: the directory to be parsed
@returns int */
int* parseDir(DIR* dir){
  struct dirent *dir_entry;
  int* toReturn; //[0] = total # of backups, [1] = current backup number
  int backup_num, month, day, hour, minute;//results from parsing backup name

  //allocate memory
  toReturn = calloc(2, sizeof(int));

  while((dir_entry = readdir(dir)) != NULL){ //foreach entry in the directory
    fprintf(stdout, "    [%s]\n", dir_entry->d_name);
    if(strstr(dir_entry->d_name, "cb_backup") != NULL){ //if entry is a backup
      toReturn[0] += 1; //tally backups
      sscanf(dir_entry->d_name, "%d_cb_backup_%02d%02d-%02d%02d", &backup_num,
      &month, &day, &hour, &minute); //parse entry name
      if(toReturn[1] < backup_num) toReturn[1] = backup_num;
      else if(toReturn[1] == backup_num){ //potential error
        fprintf(stdout, "WARNING: A backup number was found matching a "
        "previous backup number.\n This is not intended behavior and probably "
        "indicates an error or tampering within the backup folder.\n The "
        "program will proceed as normal.\n");
      }
    }
  }
  fprintf(stdout, "Found %d existing backups.\n", toReturn[0]);
  rewinddir(dir);
  return toReturn;
}
/* Helper function for purgeBackups().
Renames the given file and checks that the procedure worked properly.
@params - char* path: the path to the file directory; char* file: the name of
the file;
@returns void*/
void subPurgeBackupsRename(char* path, char* file){
  //variables
  char *old_path, *new_path;
  char* new_name = "0_cb_prev.tar.gz";
  int ret;
  //initialize
  old_path = calloc((strlen(path)+strlen(file)), sizeof(char));
  new_path = calloc((strlen(path)+strlen(new_name)), sizeof(char));
  //construct paths
  strcpy(old_path, path); strcat(old_path, file);
  fprintf(stdout, "Created old path '%s'.\n", old_path);
  strcpy(new_path, path); strcat(new_path, new_name);
  fprintf(stdout, "Created new path '%s'.\n", new_path);
  //rename
  ret = rename(old_path, new_path);
  //check rename result
  if (ret != 0){
    fprintf(stderr, "ERROR @ subPurgeBackupsRename. Exitting with error:\n"
    "    %s!\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  //free memory
  free(old_path); free(new_path);
}
/* Helper function for purgeBackups().
Removes the given file and checks that the procedure worked properly.
@params - char* path: the path to the file directory; char* file: the name of
the file;
@returns void*/
void subPurgeBackupsRemove(char* path, char* file){
  int ret;
  char* entry_handle = calloc(strlen(path)+strlen(file), sizeof(char));

  strcpy(entry_handle, path); strcat(entry_handle, file);
  ret = remove(entry_handle);
  if (ret != 0){ //check if the removal was a success
    fprintf(stderr, "ERROR @ subPurgeBackupsRemove. Exitting with error:\n"
    "    %s!\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  free(entry_handle);
}
/* Using the previously-counted number of backups and the user-given desired
number, purgeBackups() removes backups (starting at the oldest based on
prepended number) until there is one less than the desired (as one will be added
in a later subroutine).
@params - DIR* dir: directory to be purged; int cur_count: the current number of
backups; int desired_cap: the user-given backup cap;
@returns void */
void purgeBackups(char* dir_path, int cur_count, int cur_num, int desired_cap){
  int purgeLimit = FULL_PURGE; //all numbers below this will be removed
  //pL of -1 means purge all existing backups
  DIR* dir = opendir(dir_path);
  struct dirent *dir_entry;
  int loop = 0;

  //check for loop time
  if (cur_num == BACKUP_LIMIT-1) loop = 1;

  if(cur_count >= desired_cap || loop == 1){
    //set pL (no need if loop is true)
    if(desired_cap > 1 && loop != 1) purgeLimit = cur_num - (desired_cap-2);
    //EX: if the cap is 2, only the most recent (previous) backup will be kept
    fprintf(stdout, "Loop is %s, purge limit is %d\n", (loop==1)?"true":"false",
    purgeLimit);
    while((dir_entry = readdir(dir)) != NULL){ //foreach entry in the directory
      if(strstr(dir_entry->d_name, "cb_backup") != NULL){
        //backup found
        int save = 0; //delete by default
        int b_num;
        sscanf(dir_entry->d_name, "%d_", &b_num); //fetch backup number

        //if loop is true, save only the final backup
        //if loop is false, check the backup number against the purgeLimit
        if(loop == 1){ //if loop, save only latest
          if (b_num == BACKUP_LIMIT-1){
            subPurgeBackupsRename(dir_path, dir_entry->d_name);
            save = 1;
          }
        } else{ //loop is false, check against pL
          if (b_num >= purgeLimit) save = 1;
        }
        //now that the file has been identified, consider purging
        if(save == 0){
          fprintf(stdout, "    Removing backup %s\n", dir_entry->d_name);
          subPurgeBackupsRemove(dir_path, dir_entry->d_name);
        }
      }
    }
    fprintf(stdout, "Purge completed.\n");
  } else fprintf(stdout, "Purging skipped.\n");
  closedir(dir);
}
/* Builds and returns the Bash command that creates the backup.
@params - char* target: target directory, char* dest: destination directory, int
backup_num: the current backup number (to be prepended)
@returns char* */
char* constructCommand(char* target, char* dest, int backup_num, char* cur_time){
  //strings to be built
  char* command = calloc(200, 1); //callocs could be slimmed to save a few bytes
  char* backup = calloc(100, 1); //holds the name of the backup file w/in dest
  
  int command_length = 0; //holds the buffer length for the command

  //check for backup number 100
  if (backup_num == BACKUP_LIMIT){
    fprintf(stdout, "Backup limit (%d) reached, resetting to 1\n", BACKUP_LIMIT);
    backup_num = 1;
  } else if(backup_num > BACKUP_LIMIT){
    fprintf(stdout, "WARNING: Backup number (%d) is greater than limit (%d). "
    "It will be reset to 1, but this should be investigated as it suggests "
    "external tampering.\n", backup_num , BACKUP_LIMIT);
    backup_num = 1;
  }
  //prep backup path
  //The backup path is the destination path, but to an actual file
  strcpy(backup, dest);
  command_length = snprintf(backup, BUF_CAP, "%s%d_cb_backup%s.tar.gz", dest,
    backup_num, cur_time); //construct destination file and save length
  //calculate necessary buffer length
  command_length = command_length + strlen(target) +
    strlen("tar -czf " " -C " " .");
  fprintf(stdout, "Executing command construction with buffer of %d+3.\n",
    command_length);
  //construct command
  snprintf(command, command_length+3, "tar -czf %s -C %s .", backup, target);
  //^ the extra 3 bytes are cautionary
  //release memory
  free(backup);
  return command;
}
/* Main */
int main(int argc, char* argv[]){
  //variables
  int desired_cap, backup_c, cur_backup_num;
  char* dest_path, *target_path, *command;
  DIR *target_dir = NULL, *dest_dir = NULL;
  char* cur_time = calloc(10, 1), *start_str = calloc(BUF_CAP, sizeof(char));
  time_t T = time(NULL);
  struct tm *time = localtime(&T);

  //start time------------------------------------------------------------------
  strftime(start_str, BUF_CAP, "Start time is %H:%M on %A the %d of %B", time);
  fprintf(stdout, "%s\n", start_str);
  //check arguments-------------------------------------------------------------
  desired_cap = checkArgs(argc, argv);
  //target----------------------------------------------------------------------
  target_path = argv[2];
  target_dir = tryDir(target_path); //test the target directory
  //dest------------------------------------------------------------------------
  dest_path = argv[3];
  //check for a slash
  //w/o a slash, the backup will be in the current directory
  int len = (int)strlen(dest_path);
  if (dest_path[len-1] != '/'){
    fprintf(stdout, "Dest slash missing! Appending slash...\n");
    strcat(dest_path, "/");
  }
  dest_dir = tryDir(dest_path);
  //backup counting-------------------------------------------------------------
  fprintf(stdout, "Parsing destination directory...\n");
  int* temp = parseDir(dest_dir);
  backup_c = temp[0]; cur_backup_num = temp[1];
  free(temp);
  fprintf(stdout, "Newest backup found is %d\n", cur_backup_num);
  //manage number of backups----------------------------------------------------
  purgeBackups(dest_path, backup_c, cur_backup_num, desired_cap);
  //begin backup----------------------------------------------------------------
  strftime(cur_time, 10, "%m%d_%H%M", time);
  command = constructCommand(target_path, dest_path, cur_backup_num+1,
    cur_time); //backup count
  //is incremented as to not overwrite the previous backup
  fprintf(stdout, "Command constructed: %s\n", command);
  fprintf(stdout, "System call returned %d\n", system(command));
  //freeing---------------------------------------------------------------------
  closedir(target_dir);
  closedir(dest_dir);
  free(start_str);
  free(cur_time);
  free(command);
  return 0;
}
